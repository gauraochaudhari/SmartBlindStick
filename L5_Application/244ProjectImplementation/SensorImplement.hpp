/*
 * SensorImplement.hpp
 *
 *  Created on: May 10, 2016
 *      Author: Gaurav
 */

#ifndef L5_APPLICATION_244PROJECTIMPLEMENTATION_SENSORIMPLEMENT_HPP_
#define L5_APPLICATION_244PROJECTIMPLEMENTATION_SENSORIMPLEMENT_HPP_


#define DEBUG                                   0       // Set 1 enable print else 0
#define PW_CM_CONV                              58      //Division factor to convert distance in cm
#define LED_DISP_FACTOR                         10      //Factor for display of front sensor distance on LED display
#define DISTANCE_LIMIT                          255     //Limit sesnor distance
#define SENSOR_THRESH                           10      //Threshold value for deviation in successive reading

#define TIMER_TICK                              1       // 1us per tick for timer
#define SENSOR_INIT_DELAY                       250     //Time required by sensor for initial calibration
#define SONAR_TRIG_DELAY                        20      //Trigger pulse width for MB1010 sensor
#define HCSR04_TRIG_DELAY                       15      //Trigger pulse width for HC SR-04
#define SENSOR_TRIG_DELAY_FB                    55      //Delay to get reading from MB1010 sensor
#define SENSOR_TRIG_DELAY_RL                    30      //Delay to get reading from HCSR-04 sensor


//Sensor trigger and echo pins. RX indicate trigger pin while PW is PWM/Echo
#define LEFT_SENSOR_TG							0
#define LEFT_SENSOR_EC							1
#define CENTER_SENSOR_TG						2
#define CENTER_SENSOR_EC						3
#define RIGHT_SENSOR_TG							4
#define RIGHT_SENSOR_EC							5
#define LASER_SENSOR_INT						6

#define BOOTREPLYDATA                           20150607 //Bootreply data to be transmitted as Boot to master

typedef struct                  //structure to store obstacle distance by sensor
{
	uint8_t left;
	uint8_t center;
	uint8_t right ;
}sensor_data_t;

bool InitializeSensor();
bool SensorCalculation();
void CheckGroundDetection();

#endif /* L5_APPLICATION_244PROJECTIMPLEMENTATION_SENSORIMPLEMENT_HPP_ */
