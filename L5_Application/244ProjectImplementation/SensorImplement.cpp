/*
 * SensorImplement.cpp
 *
 *  Created on: May 10, 2016
 *      Author: Gaurav
 */


#include "tasks.hpp"
#include "shared_handles.h"
#include "scheduler_task.hpp"
#include "eint.h"
#include "io.hpp"
#include "lpc_timers.h"
#include "utilities.h"
#include "lpc_pwm.hpp"
#include "scheduler_task.hpp"
#include "stdio.h"
#include "LED_Display.hpp"
#include "SensorImplement.hpp"

extern uint8_t sensorActive;
int leftStartTime = 0, centerStartTime = 0, rightStartTime = 0;
int leftDistanceWidth = 0, centerDistanceWidth = 0, rightDistanceWidth = 0;
long yield = 0;
bool groundDetected = false;
bool obstacleDetected = false;
const uint8_t port2_1 = LEFT_SENSOR_EC, port2_3 = CENTER_SENSOR_EC, port2_5 = RIGHT_SENSOR_EC, port2_6 = LASER_SENSOR_INT;

sensor_data_t sensor_readings;

SemaphoreHandle_t groundDetectedSignal = NULL;
SoftTimer interruptTimer(500);

void SensorTrigger(int pin)
{
	LPC_GPIO2->FIOPIN &= ~(1 << pin);
	LPC_GPIO2->FIOPIN |= (1 << pin);
	delay_us(HCSR04_TRIG_DELAY);
	LPC_GPIO2->FIOPIN &= ~(1 << pin);

	switch(pin)
	{
	case 0:
		leftStartTime = lpc_timer_get_value(lpc_timer0);
		break;
	case 2:
		centerStartTime = lpc_timer_get_value(lpc_timer0);
		break;
	case 4:
		rightStartTime = lpc_timer_get_value(lpc_timer0);
		break;
	}
}

void LeftEcho()
{
	leftDistanceWidth = lpc_timer_get_value(lpc_timer0) - leftStartTime;
	sensor_readings.left = leftDistanceWidth/PW_CM_CONV;
	portYIELD_FROM_ISR(yield);
}

void CentreEcho()
{
	centerDistanceWidth = lpc_timer_get_value(lpc_timer0) - centerStartTime;
	sensor_readings.center = centerDistanceWidth/PW_CM_CONV;
	portYIELD_FROM_ISR(yield);
}

void RightEcho()
{
	rightDistanceWidth = lpc_timer_get_value(lpc_timer0) - rightStartTime;
	sensor_readings.right = rightDistanceWidth/PW_CM_CONV;
	portYIELD_FROM_ISR(yield);
}

void LaserSensorInterrupt()
{
	long yeild = 0;

	if (interruptTimer.expired())
	{
		xSemaphoreGiveFromISR(groundDetectedSignal, &yeild);
		interruptTimer.reset();
		portYIELD_FROM_ISR(yeild);
	}
}

void CheckGroundDetection()
{
	if(xSemaphoreTakeFromISR(groundDetectedSignal, 0))
	{
		if(!groundDetected)
		{
			groundDetected = true;
			printf("Ground Detected");
			eint3_enable_port2(port2_6, eint_rising_edge, LaserSensorInterrupt);
		}
		else
		{
			groundDetected = false;
			printf("Ground Released");
			eint3_enable_port2(port2_6, eint_falling_edge, LaserSensorInterrupt);
		}
	}
}

void sensor_distance_limit()
{
	if(sensor_readings.left > DISTANCE_LIMIT)
	{
		sensor_readings.left = DISTANCE_LIMIT;
	}
	if(sensor_readings.center > DISTANCE_LIMIT)
	{
		sensor_readings.center = DISTANCE_LIMIT;
	}
	if(sensor_readings.right > DISTANCE_LIMIT)
	{
		sensor_readings.right = DISTANCE_LIMIT;
	}
}

void InitializeEchoPins()
{
	eint3_enable_port2(port2_1, eint_falling_edge, LeftEcho);
	eint3_enable_port2(port2_3, eint_falling_edge, CentreEcho);
	eint3_enable_port2(port2_5, eint_falling_edge, RightEcho);
	eint3_enable_port2(port2_6, eint_falling_edge, LaserSensorInterrupt);
}

void InitializeSensorTrigPins()
{
	LPC_GPIO2->FIODIR |= (1 << LEFT_SENSOR_TG);
	LPC_GPIO2->FIODIR |= (1 << CENTER_SENSOR_TG);
	LPC_GPIO2->FIODIR |= (1 << RIGHT_SENSOR_TG);
}

bool InitializeSensor(void)
{
	InitializeSensorTrigPins();
	delay_ms(SENSOR_INIT_DELAY);                    	//Time required by sensor for calibration.
	InitializeEchoPins();                               //Initialize interrupts for reading sensor pins
	lpc_timer_enable(lpc_timer0, TIMER_TICK);       	//Enable timer with tick 1us
	vSemaphoreCreateBinary(groundDetectedSignal);
	return true;
}

bool SensorCalculation()
{

	SensorTrigger(LEFT_SENSOR_TG);			//Trigger left sensor
	delay_ms(SENSOR_TRIG_DELAY_RL);
	SensorTrigger(CENTER_SENSOR_TG); 	    //Trigger center sensor
	delay_ms(SENSOR_TRIG_DELAY_RL);
	SensorTrigger(RIGHT_SENSOR_TG);     	//Trigger right sensor
	delay_ms(SENSOR_TRIG_DELAY_RL);

#if DEBUG
	//printf("%d %d %d\n",sensor_readings.left, sensor_readings.center, sensor_readings.right);
#endif

	if((sensor_readings.left < 60 || sensor_readings.center < 100 || sensor_readings.right < 60) && !groundDetected)
	{
		sensorActive = 1;
		obstacleDetected = true;
	}
	else
	{
		sensorActive = 0;
		obstacleDetected = false;
	}

	sensor_distance_limit();

	return true;
}


