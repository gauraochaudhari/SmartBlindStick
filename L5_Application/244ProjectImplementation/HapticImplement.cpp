/*
 * HapticImplement.cpp
 *
 *  Created on: May 10, 2016
 *      Author: Gaurav
 */

#include "HapticImplement.hpp"
#include "i2c2.hpp"
#include "i2c2_device.hpp"
#include "io.hpp"
#include "stdio.h"
#include "gpio.hpp"

uint8_t sensorActive = 0;
extern bool obstacleDetected;

void SetDriverEnables()
{
	GPIO pin1_29(P1_29);
	pin1_29.setAsOutput();
	pin1_29.setLow();

	GPIO pin1_28(P1_28);
	pin1_28.setAsOutput();
	pin1_28.setLow();

	GPIO pin1_23(P1_23);
	pin1_23.setAsOutput();
	pin1_23.setHigh();
}


I2CHapTask::I2CHapTask(uint8_t priority) : scheduler_task("I2CHapTask", 2000, priority)
{

}

bool I2CHapTask::haptic_driver_init()
{
	hapdrivevalue=0x00;
	i2cobj.writeReg(slaveAddr,DRV2605_REG_MODE,hapdrivevalue);
	i2cobj.writeReg(slaveAddr,DRV2605_REG_RTPIN,hapdrivevalue);
	hapdrivevalue=0x01;
	i2cobj.writeReg(slaveAddr,DRV2605_REG_WAVESEQ1,hapdrivevalue);
	hapdrivevalue=0x00;
	i2cobj.writeReg(slaveAddr,DRV2605_REG_WAVESEQ2,hapdrivevalue);
	i2cobj.writeReg(slaveAddr,DRV2605_REG_OVERDRIVE,hapdrivevalue);
	i2cobj.writeReg(slaveAddr,DRV2605_REG_SUSTAINPOS,hapdrivevalue);
	i2cobj.writeReg(slaveAddr,DRV2605_REG_SUSTAINNEG,hapdrivevalue);
	i2cobj.writeReg(slaveAddr,DRV2605_REG_BREAK,hapdrivevalue);
	hapdrivevalue=0x64;
	i2cobj.writeReg(slaveAddr,DRV2605_REG_AUDIOMAX,hapdrivevalue);
	hapreadvalue = i2cobj.readReg(slaveAddr,DRV2605_REG_FEEDBACK);
//	printf("%x", hapreadvalue);
	hapdrivevalue = hapreadvalue & 0x7f;
	i2cobj.writeReg(slaveAddr,DRV2605_REG_FEEDBACK,hapdrivevalue);
	hapreadvalue = i2cobj.readReg(slaveAddr,DRV2605_REG_CONTROL3);
	hapdrivevalue = hapreadvalue | 0x20;
	i2cobj.writeReg(slaveAddr,DRV2605_REG_CONTROL3,hapdrivevalue);
	return true;

}

bool I2CHapTask::hapWrite(uint8_t slot, uint8_t value, hapticfunction hvalue)
{

	switch(hvalue){

	case SETWAVEFORM:
		hapdrivevalue = value;
		i2cobj.writeReg(slaveAddr,DRV2605_REG_WAVESEQ1+slot,hapdrivevalue);
		break;
	case SELECTLIBRARY:
		hapdrivevalue = value;
		i2cobj.writeReg(slaveAddr,DRV2605_REG_LIBRARY,hapdrivevalue);
		break;
	case GO:
		hapdrivevalue = value;
		i2cobj.writeReg(slaveAddr,DRV2605_REG_GO,hapdrivevalue);
		break;
	case SETMODE:
		hapdrivevalue = value;
		i2cobj.writeReg(slaveAddr,DRV2605_REG_MODE,hapdrivevalue);
		break;
	case SETREALTIMEVALUE:
		hapdrivevalue = value;
		i2cobj.writeReg(slaveAddr,DRV2605_REG_RTPIN,hapdrivevalue);
		break;
	}

	return true;
}

bool I2CHapTask::init(void)
{
	i2cobj.initSlave(slaveAddr, buffer, sizeof(buffer));
	haptic_driver_init();
	hapWrite(0,1,SELECTLIBRARY);
	hapWrite(0,DRV2605_MODE_INTTRIG,SETMODE);
	return true;
}

bool I2CHapTask::run(void *p)
{
	uint8_t counter = 5;
	int i;

	if(sensorActive == 1 && obstacleDetected)
	{
		for(i = 0; i < counter; i++)
		{
			hapWrite(0,1,SETWAVEFORM);
			hapWrite(1,0,SETWAVEFORM);
			hapWrite(0,1,GO);
			vTaskDelay(2000);
			hapWrite(1,0,GO);
			LE.toggle(1);
		}
		sensorActive = 1;
	}
	else if(sensorActive == 2 && obstacleDetected)
	{
		for(i = 0; i < counter; i++)
		{
			hapWrite(0,1,SETWAVEFORM);
			hapWrite(1,0,SETWAVEFORM);
			hapWrite(0,1,GO);
			vTaskDelay(2000);
			hapWrite(1,0,GO);
		}
		sensorActive = 0;

	}
	else if(sensorActive == 3 && obstacleDetected)
	{
		for(i = 0; i < counter; i++)
		{
			hapWrite(0,1,SETWAVEFORM);
			hapWrite(1,0,SETWAVEFORM);
			hapWrite(0,1,GO);
			vTaskDelay(2000);
			hapWrite(1,0,GO);
		}
		sensorActive = 0;
	}
	else
	{

	}
	return true;
}



