/*
 * HapticImplement.hpp
 *
 *  Created on: May 10, 2016
 *      Author: Gaurav
 */

#ifndef L5_APPLICATION_244PROJECTIMPLEMENTATION_HAPTICIMPLEMENT_HPP_
#define L5_APPLICATION_244PROJECTIMPLEMENTATION_HAPTICIMPLEMENT_HPP_


#include "scheduler_task.hpp"
#include "i2c2.hpp"

static uint8_t hapdrivevalue;
static uint8_t hapreadvalue;

#define DRV2605_REG_STATUS 0x00
#define DRV2605_REG_MODE 0x01
#define DRV2605_MODE_INTTRIG  0x00
#define DRV2605_MODE_EXTTRIGEDGE  0x01
#define DRV2605_MODE_EXTTRIGLVL  0x02
#define DRV2605_MODE_PWMANALOG  0x03
#define DRV2605_MODE_AUDIOVIBE  0x04
#define DRV2605_MODE_REALTIME  0x05
#define DRV2605_MODE_DIAGNOS  0x06
#define DRV2605_MODE_AUTOCAL  0x07
#define DRV2605_REG_RTPIN 0x02
#define DRV2605_REG_LIBRARY 0x03
#define DRV2605_REG_WAVESEQ1 0x04
#define DRV2605_REG_WAVESEQ2 0x05
#define DRV2605_REG_WAVESEQ3 0x06
#define DRV2605_REG_WAVESEQ4 0x07
#define DRV2605_REG_WAVESEQ5 0x08
#define DRV2605_REG_WAVESEQ6 0x09
#define DRV2605_REG_WAVESEQ7 0x0A
#define DRV2605_REG_WAVESEQ8 0x0B
#define DRV2605_REG_GO 0x0C
#define DRV2605_REG_OVERDRIVE 0x0D
#define DRV2605_REG_SUSTAINPOS 0x0E
#define DRV2605_REG_SUSTAINNEG 0x0F
#define DRV2605_REG_BREAK 0x10
#define DRV2605_REG_AUDIOCTRL 0x11
#define DRV2605_REG_AUDIOLVL 0x12
#define DRV2605_REG_AUDIOMAX 0x13
#define DRV2605_REG_RATEDV 0x16
#define DRV2605_REG_CLAMPV 0x17
#define DRV2605_REG_AUTOCALCOMP 0x18
#define DRV2605_REG_AUTOCALEMP 0x19
#define DRV2605_REG_FEEDBACK 0x1A
#define DRV2605_REG_CONTROL1 0x1B
#define DRV2605_REG_CONTROL2 0x1C
#define DRV2605_REG_CONTROL3 0x1D
#define DRV2605_REG_CONTROL4 0x1E
#define DRV2605_REG_VBAT 0x21
#define DRV2605_REG_LRARESON 0x22

typedef enum hapticfunction{
	SETWAVEFORM = 0,
	SELECTLIBRARY,
	GO,
	SETMODE,
	SETREALTIMEVALUE
};

void SetDriverEnables();

class I2CHapTask : public scheduler_task
{
	public:
		I2CHapTask(uint8_t priority);
		bool haptic_driver_init();
		bool hapWrite(uint8_t slot, uint8_t value, hapticfunction hvalue);
		bool init(void);
		bool run(void *p);

	private:
		I2C2 &i2cobj = I2C2::getInstance();;
		const uint8_t slaveAddr = 0xb4;
		uint8_t buffer[20] = { 0 };
};



#endif /* L5_APPLICATION_244PROJECTIMPLEMENTATION_HAPTICIMPLEMENT_HPP_ */
