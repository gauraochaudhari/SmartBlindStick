/*
 * keylocator_tx.cpp
 *
 *  Created on: May 22, 2016
 *      Author: Gaurav
 */

#include "keylocator_client/keylocator_tx.hpp"
#include <stdio.h>
#include <stdint.h>
#include <io.hpp>
#include <eint.h>
#include "soft_timer.hpp"

SemaphoreHandle_t keyLocatorHandle = NULL;
SoftTimer keyLocatorTimer(50);

void KeyLocatorCallback()
{
	long yeild = 0;

	if (keyLocatorTimer.expired())
	{
		xSemaphoreGiveFromISR(keyLocatorHandle, &yeild);
		keyLocatorTimer.reset();
		portYIELD_FROM_ISR(yeild);
	}

}

void KeyLocator_Initialize()
{
	const uint8_t port0_29 = KEY_LOCATOR_PIN;
	eint3_enable_port0(port0_29, eint_falling_edge, KeyLocatorCallback);
	vSemaphoreCreateBinary(keyLocatorHandle);
}

keyTransmitTask::keyTransmitTask(uint8_t priority) :
scheduler_task("txTask", 8 * 512, PRIORITY_HIGH)
{
	;
}

bool keyTransmitTask:: run(void *p)
{
	if(xSemaphoreTakeFromISR(keyLocatorHandle, 0))
	{
		keypress();
	}

	return true;
}

bool keyTransmitTask::keypress(void){

	char hops = 1;
	char addr = 106;
	int keyPressed=0;
	mesh_packet_t pkt;

	//Send a packet with one data variable
	mesh_form_pkt(&pkt, addr, mesh_pkt_ack, hops,
			1,                      				// 1 Parameter in the Packet
			&keyPressed, sizeof(keyPressed));     //Parameter = keyPressed

	//Packet was formed above, now send it
	mesh_send_formed_pkt(&pkt);

	return true;
}


