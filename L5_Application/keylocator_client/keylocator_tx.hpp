/*
 * keylocator_tx.hpp
 *
 *  Created on: May 22, 2016
 *      Author: Gaurav
 */

#ifndef L5_APPLICATION_KEYLOCATOR_CLIENT_KEYLOCATOR_TX_HPP_
#define L5_APPLICATION_KEYLOCATOR_CLIENT_KEYLOCATOR_TX_HPP_

#include "scheduler_task.hpp"
#include "FreeRTOS.h"
#include "wireless.h"
#include <stdlib.h>

#define KEY_LOCATOR_PIN			29

void KeyLocator_Initialize();

class keyTransmitTask : public scheduler_task
{
    public:
		keyTransmitTask(uint8_t priority);   ///< Constructor
		bool run(void *p);              ///< The main loop

    private:
        bool keypress(void);
};



#endif /* L5_APPLICATION_KEYLOCATOR_CLIENT_KEYLOCATOR_TX_HPP_ */
