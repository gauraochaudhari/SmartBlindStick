/*
 * GSMInterfaceModule.cpp
 *
 *  Created on: May 22, 2016
 *      Author: Gaurav
 */

#include "GSMInterfaceModule.hpp"
#include "utilities.h"
#include "uart3.hpp"
#include "eint.h"
#include "soft_timer.hpp"

SemaphoreHandle_t panicMessageHandle = NULL;
SoftTimer panicMessageTimer(50);
char controlZ = 0x1A;

void PanicMessageCallBack()
{
	long yeild = 0;

	if (panicMessageTimer.expired())
	{
		xSemaphoreGiveFromISR(panicMessageHandle, &yeild);
		panicMessageTimer.reset();
		portYIELD_FROM_ISR(yeild);
	}

}

void GSM_Initialize()
{
	const uint8_t port2_7 = PANIC_MESSAGE_KEY;
	eint3_enable_port2(port2_7, eint_falling_edge, PanicMessageCallBack);
	vSemaphoreCreateBinary(panicMessageHandle);
	write_to_sim();
}

void send_from_storage(void)
{
	Uart3 &msz = Uart3::getInstance();
	msz.init(115200, 512, 512);
	msz.putline(at_init); //second param of put/putChar is optional, defaults to portMAX_DELAY
	delay_ms(2000);
	msz.putline(sms_mode);
	delay_ms(150);
	msz.putline(pref_sms_sto);
	delay_ms(150);
	msz.putline(sms_address);
	delay_ms(150);
	msz.putline(send_from_sto);
	delay_ms(200);
	msz.putline(send_sto_multi1);
	delay_ms(200);
	msz.putline(send_sto_multi2);
	delay_ms(200);
	msz.putline(send_sto_multi3);
	delay_ms(200);

}

void write_to_sim(void)
{
	Uart3 &msz = Uart3::getInstance();
	msz.init(115200, 512, 512);
	msz.putline(at_init); //second param of put/putChar is optional, defaults to portMAX_DELAY
	delay_ms(2000);
	msz.putline(pref_sms_sto);
	delay_ms(150);
	msz.putline(write_sms_sto);
	delay_ms(150);
	msz.putline(panic_msz);
	delay_ms(150);
	msz.putChar(controlZ); //After writing message to storage, press ctrl-z
	delay_ms(350);
}

void my_sms(void){
	Uart3 &msz = Uart3::getInstance();
	msz.init(115200, 512, 512);
	msz.putline(at_init); //second param of put/putChar is optional, defaults to portMAX_DELAY
	delay_ms(2000);
	msz.putline(sms_mode);
	vTaskDelay(500);
	msz.putline(subscribed_phone);
	vTaskDelay(500);

	msz.putline(panic_msz);
	msz.putChar(controlZ);
	vTaskDelay(500);
}

void delete_sto_all(void)
{
	Uart3 &msz = Uart3::getInstance();
	msz.init(115200, 512, 512);
	msz.putline(at_init); //second param of put/putChar is optional, defaults to portMAX_DELAY
	delay_ms(2000);
	msz.putline(delete_from_sto);
	delay_ms(150);
}

void SendMessage()
{
	if(xSemaphoreTakeFromISR(panicMessageHandle, 0))
	{
		send_from_storage();
	}
}
