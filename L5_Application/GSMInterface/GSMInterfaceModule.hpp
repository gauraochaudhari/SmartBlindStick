/*
 * GSMInterfaceModule.hpp
 *
 *  Created on: May 22, 2016
 *      Author: Gaurav
 */

#ifndef L5_APPLICATION_GSMINTERFACE_GSMINTERFACEMODULE_HPP_
#define L5_APPLICATION_GSMINTERFACE_GSMINTERFACEMODULE_HPP_

#define PANIC_MESSAGE_KEY			7
const char at_init[] = { "AT\r" };
const char sms_mode[] = { "AT+CMGF=1\r" };
const char phone_mode[] = {"AT+CMGF=0\r"};
const char subscribed_phone[] = { "AT+CMGS=\"15308447161\"\r" };
const char panic_msz[] = {"It is an emergency! Please reach to me ASAP"};
const char char_set[] = {"AT+CSCS=\"GSM\"\r"};
const char reg_state[]= {"AT+CREG=2\r"};
const char chk_reg[]= {"AT+CREG=?\r"};
const char network_op[]= {"AT+COPS=?\r"};
const char read_card[] = {"AT + CPIN?\r"};
const char make_call[] = {"ATD15308447161;\r"};
const char ready_for_call [] = {"AT+CCALR=?\r"};
const char report_ME_err[] = {"AT+CMEE=2\r"};

const char pref_sms_sto[] = { "AT+CPMS=\"SM\",\"SM\"\r" };
const char sms_address[] = { "AT+CSCA=\"+12063130004\"\r" };  //message center address
const char write_sms_sto[] = { "AT+CMGW=\"15308447161\",129,\"STO UNSENT\"\r" };
const char send_from_sto[] = { "AT+CMSS=1,\"15308447161\"\r" };
const char send_sto_multi1[] = { "AT+CMSS=1,\"15308447161\"\r" };
const char send_sto_multi2[] = { "AT+CMSS=1,\"16692388693\"\r" };
const char send_sto_multi3[] = { "AT+CMSS=1,\"15308447161\"\r" };
const char read_sms_sto[] = { "AT+CMGR=1" };
const char delete_from_sto[] = { "AT+CMGD=1,4\r" };



void GSM_Initialize();
void send_from_storage(void);
void write_to_sim(void);
void my_sms(void);
void delete_sto_all(void);
void SendMessage();



#endif /* L5_APPLICATION_GSMINTERFACE_GSMINTERFACEMODULE_HPP_ */
