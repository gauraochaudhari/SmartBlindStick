/*
 * SemaphoreTaskHW.cpp
 *
 *  Created on: Apr 4, 2016
 *      Author: Gaurav
 */

#include "SemaphoreTask/SemaphoreTaskHW.hpp"
#include "semphr.h"
#include "stdio.h"
#include "rtc.h"
#include "soft_timer.hpp"
#include <iostream>

using namespace std;

SemaphoreHandle_t switchSignal = NULL;
SoftTimer myTimer(500);

void InterruptCallback(void)
{
	long yeild = 0;

	if (myTimer.expired())
	{
		printf("\nSemaphore Sent.");
		xSemaphoreGiveFromISR(switchSignal, &yeild);
		myTimer.reset();
		portYIELD_FROM_ISR(yeild);
	}
}

SemaphoreTask::SemaphoreTask(uint8_t priority) : scheduler_task("semaphoreTask", 1000, priority)
{

}

bool SemaphoreTask::init(void)
{
	uint8_t port0_pin29 = 29;
	eint3_enable_port0(port0_pin29, eint_falling_edge, InterruptCallback);
	vSemaphoreCreateBinary(switchSignal);
	return true;
}

bool SemaphoreTask::run(void *p)
{
	if(xSemaphoreTakeFromISR(switchSignal, 0))
	{
		printf("\nSemaphore Received.");
	}
	return true;
}
