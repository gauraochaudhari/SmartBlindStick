/*
 * SemaphoreTaskHW.hpp
 *
 *  Created on: Apr 4, 2016
 *      Author: Gaurav
 */

#ifndef L5_APPLICATION_SEMAPHORETASK_SEMAPHORETASKHW_HPP_
#define L5_APPLICATION_SEMAPHORETASK_SEMAPHORETASKHW_HPP_


#include "eint.h"
#include "scheduler_task.hpp"
#include "gpio.hpp"


class SemaphoreTask : public scheduler_task
{
	public:
		SemaphoreTask(uint8_t priority);
		bool init(void);
		bool run(void *p);
};


#endif /* L5_APPLICATION_SEMAPHORETASK_SEMAPHORETASKHW_HPP_ */
