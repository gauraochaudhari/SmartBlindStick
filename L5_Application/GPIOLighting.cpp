/*
 * GPIOLighting.cpp
 *
 *  Created on: Feb 14, 2016
 *      Author: Gaurav
 */

#include "GPIOLighting.hpp"
#include "io.hpp"
#include "eint.h"
#include "stdio.h"

extern int ledTimeDuration;
extern bool alternate;
extern bool terminalReady;

void LEDOnPort2(uint8_t pinNum)
{
	LPC_GPIO2->FIOSET |= (1 << pinNum);
}

void LEDOffPort2(uint8_t pinNum)
{
	LPC_GPIO2->FIOCLR |= (1 << pinNum);
}

void ToggleLEDPort2(uint8_t pinNum)
{
	LPC_GPIO2->FIOPIN ^= (1 << pinNum);
}

GPIOLightingTask::GPIOLightingTask(uint8_t priority): scheduler_task("gpioTask", 512, priority)
{
	gpioReadSwitch = 0;
}

bool GPIOLightingTask::init(void)
{
	// Setting the pin as an input for switch
	LPC_GPIO2->FIODIR &= ~(1 << 0);

	// Setting the pin mode to enable pull down register
	LPC_PINCON->PINMODE4 |= (3 << 0);

	// Setting the pins as an output for LED
	LPC_GPIO2->FIODIR |= (1 << 1);
	LPC_GPIO2->FIODIR |= (1 << 2);
	LPC_GPIO2->FIODIR |= (1 << 3);
	LPC_GPIO2->FIODIR |= (1 << 4);

	return true;
}

bool GPIOLightingTask::run(void *p)
{
	gpioReadSwitch = LPC_GPIO2->FIOPIN & (1 << 0);
	if(!terminalReady)
	{
		if(gpioReadSwitch)
		{
			LEDOnPort2(1);
			LEDOnPort2(2);
			LEDOnPort2(3);
			LEDOnPort2(4);
		}
		else
		{
			LEDOffPort2(1);
			LEDOffPort2(2);
			LEDOffPort2(3);
			LEDOffPort2(4);
		}
	}
	return true;
}

LEDSchedulingTask::LEDSchedulingTask(uint8_t priority) : scheduler_task("ledSchedulingTask", 512, priority)
{
	ledTimeDuration = 100;
}

bool LEDSchedulingTask::init(void)
{
	// Setting the pins as an output for LED
	LPC_GPIO2->FIODIR |= (1 << 1);
	LPC_GPIO2->FIODIR |= (1 << 2);
	LPC_GPIO2->FIODIR |= (1 << 3);
	LPC_GPIO2->FIODIR |= (1 << 4);

	return true;
}

bool LEDSchedulingTask::run(void *p)
{
	setRunDuration(ledTimeDuration);

	if(terminalReady)
	{
		if(alternate)
		{
			LE.toggle(1);
			vTaskDelay(ledTimeDuration);
			LE.toggle(2);
			vTaskDelay(ledTimeDuration);
			LE.toggle(3);
			vTaskDelay(ledTimeDuration);
			LE.toggle(4);
			vTaskDelay(ledTimeDuration);
			ToggleLEDPort2(1);
			vTaskDelay(ledTimeDuration);
			ToggleLEDPort2(2);
			vTaskDelay(ledTimeDuration);
			ToggleLEDPort2(3);
			vTaskDelay(ledTimeDuration);
			ToggleLEDPort2(4);
		}
		else
		{
			LE.toggle(1);
			LE.toggle(2);
			LE.toggle(3);
			LE.toggle(4);
			ToggleLEDPort2(1);
			ToggleLEDPort2(2);
			ToggleLEDPort2(3);
			ToggleLEDPort2(4);
		}
	}
	return true;
}
