/*
 * GPIOLighting.hpp
 *
 *  Created on: Feb 14, 2016
 *      Author: Gaurav
 */

#ifndef L5_APPLICATION_GPIOLIGHTING_HPP_
#define L5_APPLICATION_GPIOLIGHTING_HPP_


#include "iostream"
#include "stdlib.h"
#include "scheduler_task.hpp"
#include "LPC17xx.h"

class GPIOLightingTask : public scheduler_task
{
	public:
		GPIOLightingTask(uint8_t priority);
		bool run(void *p);
		bool init(void);

	private:
		uint32_t gpioReadSwitch;
};

class LEDSchedulingTask : public scheduler_task
{
	public:
		LEDSchedulingTask();
		LEDSchedulingTask(uint8_t priority);
		bool init(void);
		bool run(void *p);
//		void SetLEDRunDuration(int ledRunDuration);
};

#endif /* L5_APPLICATION_GPIOLIGHTING_HPP_ */
